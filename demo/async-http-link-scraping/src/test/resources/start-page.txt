<!DOCTYPE html><html lang="sv"><head><base href="/"><title>Kurser för programmerare - Ribomation</title><meta
name="description" content="Stenhårda kurser för tuffa programmerare"><link rel="alternate" hreflang="en"
href="https://www.ribomation.com/index.html"><link rel="alternate" hreflang="x-default"
href="https://www.ribomation.com/index.html"><meta charset="utf-8"><meta name="viewport"
content="width=device-width,initial-scale=1,shrink-to-fit=no"><meta http-equiv="x-ua-compatible" content="ie=edge"><link
rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"><link rel="stylesheet"
href="/css/bootstrap.min.css"><link rel="stylesheet" href="/css/mdb.min.css"><link rel="stylesheet" href="/css/site.css"><link
href="/css/front.css" rel="stylesheet"></head><body class="fixed-sn light-blue-skin"><header><div id="slide-out"
class="side-nav sidebar-bg-2 fixed"><ul class="custom-scrollbar"><li class="logo-sn waves-effect"><div class="text-center"><a
href="/index.html" class="pl-0"><img src="/img/ribomation.png" alt="" class="img-fluid"></a></div></li><li><form
class="search-form" role="search"><div class="form-group md-form mt-0 pt-1 waves-light"><input id="search-course" type="text"
class="form-control" placeholder="Sök..."></div></form></li><li id="courses-heading">Våra Kurser</li><li id="courses-list"><ul
class="collapsible collapsible-accordion"><li><a title="Kurser i agil programutveckling"
class="topic-name collapsible-header waves-effect arrow-r"><i class="fa fa-graduation-cap"></i> Agile Dev. <i
class="fa fa-angle-down rotate-icon"></i></a><div class="collapsible-body"><ul><li><a href="/courses/agile/index.html"
title="Kurser i agil programutveckling" class="course waves-effect">Översikt av 3 kurser</a></li><li><a
href="/courses/agile/clean-code.html" title="Kurs i hur du systematiskt rensar i befintlig programkod"
class="course waves-effect">Clean Code</a></li><li><a href="/courses/agile/design-patterns.html"
title="Kurs i moderna designmönster i Java" class="course waves-effect">Design Patterns</a></li><li><a
href="/courses/agile/scrum.html" title="Kurs i applikationsutveckling med Scrum projektmetoden" class="course waves-effect">
Scrum</a></li></ul></div></li><li><a title="Kurser om verktyg för applikationsbyggnation"
class="topic-name collapsible-header waves-effect arrow-r"><i class="fa fa-graduation-cap"></i> Build Tools <i
class="fa fa-angle-down rotate-icon"></i></a><div class="collapsible-body"><ul><li><a href="/courses/build/index.html"
title="Kurser om verktyg för applikationsbyggnation" class="course waves-effect">Översikt av 6 kurser</a></li><li><a
href="/courses/build/ant.html" title="Kurs i byggnation av Java applikationer med Apache Ant" class="course waves-effect">Ant
</a></li><li><a href="/courses/build/cmake.html" title="Kurs i byggnation av C/C++ applikationer med CMake"
class="course waves-effect">CMake</a></li><li><a href="/courses/build/gradle.html"
title="Kurs i byggnation av Java, Groovy, JVM applikationer med Gradle" class="course waves-effect">Gradle</a></li><li><a
href="/courses/build/gulpjs.html" title="Kurs i applikationsbyggnation med GulpJS, såväl client som server side"
class="course waves-effect">GulpJS</a></li><li><a href="/courses/build/make.html"
title="Kurs i byggnation av C/C++ applikationer med Make" class="course waves-effect">Make</a></li><li><a
href="/courses/build/maven.html" title="Kurs i byggnation av Java, JVM applikationer med Maven" class="course waves-effect">
Maven</a></li></ul></div></li><li><a title="Kurser i C programmering, POSIX Threads, Linux Systemprogrammering"
class="topic-name collapsible-header waves-effect arrow-r"><i class="fa fa-graduation-cap"></i> C <i
class="fa fa-angle-down rotate-icon"></i></a><div class="collapsible-body"><ul><li><a href="/courses/c/index.html"
title="Kurser i C programmering, POSIX Threads, Linux Systemprogrammering" class="course waves-effect">Översikt av 3 kurser</a>
</li><li><a href="/courses/c/c-basics.html" title="Grundkurs i modern C programmering" class="course waves-effect">C</a></li>
<li><a href="/courses/c/c-systems-programming.html"
title="Omfattande kurs i fork/exec, pipe/dup2, tcp sockets, memory mapped I/O och mycket annat" class="course waves-effect">
Linux Sys. Progr. in C</a></li><li><a href="/courses/c/c-threads.html" title="Kurs i POSIX Threads med C programmering"
class="course waves-effect">POSIX Threads</a></li></ul></div></li><li><a title="Kurser i Modern C++, baserade på C++ 11/14/17"
class="topic-name collapsible-header waves-effect arrow-r"><i class="fa fa-graduation-cap"></i> C++ <i
class="fa fa-angle-down rotate-icon"></i></a><div class="collapsible-body"><ul><li><a href="/courses/cxx/index.html"
title="Kurser i Modern C++, baserade på C++ 11/14/17" class="course waves-effect">Översikt av 8 kurser</a></li><li><a
href="/courses/cxx/cxx-17.html" title="Kurs om nya standarden C++ 11/14/17 avseende både språk och bibliotek"
class="course waves-effect">C++ 11/14/17</a></li><li><a href="/courses/cxx/cxx-basics.html"
title="Grundkurs i Modern C++, baserat på nya standarden C++ 11/14/17" class="course waves-effect">C++ Basics</a></li><li><a
href="/courses/cxx/cxx-basics-4-c.html"
title="Grundkurs i Modern C++, baserat på nya standarden C++ 11/14/17, riktad till C programmerare" class="course waves-effect">
C++ Basics (C)</a></li><li><a href="/courses/cxx/cxx-basics-4-java.html"
title="Grundkurs i Modern C++, baserat på nya standarden C++ 11/14/17, riktad till Java programmerare"
class="course waves-effect">C++ Basics (Java)</a></li><li><a href="/courses/cxx/cxx-unit-testing.html"
title="Kurs i att skriva C++ enhetstest med Google Test" class="course waves-effect">C++ Google Test</a></li><li><a
href="/courses/cxx/cxx-supplementary.html"
title="Påbyggnadskurs i C++, med fördjuping i språket och dess bibliotek, med fokus på Modern C++" class="course waves-effect">
C++ Supplementary</a></li><li><a href="/courses/cxx/cxx-threads.html"
title="Kurs i C++ threads programmering baserat på såväl POSIX Threads och nya standarden C++ 11/14/17"
class="course waves-effect">C++ Threads</a></li><li><a href="/courses/cxx/cxx-systems-programming.html"
title="Kurs i Linux systemprogrammering med Modern C++" class="course waves-effect">Linux Sys. Progr. (C++)</a></li></ul></div>
</li><li><a title="Kurser i cloud computing med inriktning på ledande molnleverantörer"
class="topic-name collapsible-header waves-effect arrow-r"><i class="fa fa-graduation-cap"></i> Cloud Computing <i
class="fa fa-angle-down rotate-icon"></i></a><div class="collapsible-body"><ul><li><a href="/courses/cloud/index.html"
title="Kurser i cloud computing med inriktning på ledande molnleverantörer" class="course waves-effect">Översikt av 2 kurser</a>
</li><li><a href="/courses/cloud/aws-overview.html" title="Kurs i Amazon AWS, med inriktning på att snabbt komma igång"
class="course waves-effect">Amazon AWS</a></li><li><a href="/courses/cloud/firebase.html"
title="Kurs i Google Firestore, Cloud Functions, Storage, Hosting och övrigra Firebase tjänster" class="course waves-effect">
Google Firebase</a></li></ul></div></li><li><a title="Kurser om verktyg för modern programutveckling"
class="topic-name collapsible-header waves-effect arrow-r"><i class="fa fa-graduation-cap"></i> Dev. Tools <i
class="fa fa-angle-down rotate-icon"></i></a><div class="collapsible-body"><ul><li><a href="/courses/tools/index.html"
title="Kurser om verktyg för modern programutveckling" class="course waves-effect">Översikt av 3 kurser</a></li><li><a
href="/courses/tools/shell-scripts.html"
title="Kurs i att skriva BASH shell scripts i Linux, med fokus på installation och driftsättning" class="course waves-effect">
BASH Shell Scripts</a></li><li><a href="/courses/tools/git.html" title="Kurs i dagliga använding av GIT för programutveckling"
class="course waves-effect">GIT</a></li><li><a href="/courses/tools/jenkins.html"
title="Kurs i att konfigurera Jenkins som continuous integration (CI) och deployment (CD) server" class="course waves-effect">
Jenkins</a></li></ul></div></li><li><a title="Kurser om programmering i Groovy, Grails, Spock med mera"
class="topic-name collapsible-header waves-effect arrow-r"><i class="fa fa-graduation-cap"></i> Groovy <i
class="fa fa-angle-down rotate-icon"></i></a><div class="collapsible-body"><ul><li><a href="/courses/groovy/index.html"
title="Kurser om programmering i Groovy, Grails, Spock med mera" class="course waves-effect">Översikt av 3 kurser</a></li><li><a
 href="/courses/groovy/grails.html" title="Kurs i att snabbt bygga webbapplikationer med Groovy och Grails"
class="course waves-effect">Grails</a></li><li><a href="/courses/groovy/groovy.html" title="Kurs i Groovy programmering"
class="course waves-effect">Groovy</a></li><li><a href="/courses/groovy/spock.html"
title="Kurs i att skriva enhetstester med Groovy Spock" class="course waves-effect">Spock</a></li></ul></div></li><li><a
title="Kurser i Java programmering baserat på de senaste versionerna av språket"
class="topic-name collapsible-header waves-effect arrow-r"><i class="fa fa-graduation-cap"></i> Java <i
class="fa fa-angle-down rotate-icon"></i></a><div class="collapsible-body"><ul><li><a href="/courses/java/index.html"
title="Kurser i Java programmering baserat på de senaste versionerna av språket" class="course waves-effect">
Översikt av 6 kurser</a></li><li><a href="/courses/java/java-10.html"
title="Kurs om nyheterna i Java 8/9/10/... såsom lambda, data-streams, modules" class="course waves-effect">Java 8/../11</a>
</li><li><a href="/courses/java/java-basics.html" title="Grundkurs i Java programmering" class="course waves-effect">Java Basics
</a></li><li><a href="/courses/java/java-intermediate.html" title="Påbyggnadskurs i Java programmering"
class="course waves-effect">Java Intermediate</a></li><li><a href="/courses/java/java-rest-ws.html"
title="Kurs i att skapa REST Web Services med Java" class="course waves-effect">Java REST-WS</a></li><li><a
href="/courses/java/java-threads.html" title="Kurs i programmering med trådar i Java" class="course waves-effect">Java Threads
</a></li><li><a href="/courses/java/junit.html" title="Kurs i att skriva enhetstester med Java JUnit"
class="course waves-effect">JUnit Tests</a></li></ul></div></li><li><a title="Kurser om JavaScript, TypeScript och NodeJS"
class="topic-name collapsible-header waves-effect arrow-r"><i class="fa fa-graduation-cap"></i> JavaScript <i
class="fa fa-angle-down rotate-icon"></i></a><div class="collapsible-body"><ul><li><a href="/courses/javascript/index.html"
title="Kurser om JavaScript, TypeScript och NodeJS" class="course waves-effect">Översikt av 3 kurser</a></li><li><a
href="/courses/javascript/javascript.html" title="Kurs i JavaScript programmering för både client-side och server-side"
class="course waves-effect">JavaScript</a></li><li><a href="/courses/javascript/nodejs.html"
title="Kurs i hur du bygger server-side applikationer med NodeJS" class="course waves-effect">NodeJS</a></li><li><a
href="/courses/javascript/typescript.html" title="Kurs i TypeScript programmering för både client och server side"
class="course waves-effect">TypeScript</a></li></ul></div></li><li><a title="Kurser om app-utveckling för smartphones"
class="topic-name collapsible-header waves-effect arrow-r"><i class="fa fa-graduation-cap"></i> Mobile Apps Dev. <i
class="fa fa-angle-down rotate-icon"></i></a><div class="collapsible-body"><ul><li><a href="/courses/mobile/index.html"
title="Kurser om app-utveckling för smartphones" class="course waves-effect">Översikt av 2 kurser</a></li><li><a
href="/courses/mobile/android.html" title="Kurs i Android App Utveckling" class="course waves-effect">Android</a></li><li><a
href="/courses/mobile/ionic.html" title="Kurs i implementation av hybrid mobil-appar med Ionic/Angular"
class="course waves-effect">Ionic</a></li></ul></div></li><li><a title="Kurser om programmering i Erlang och Perl"
class="topic-name collapsible-header waves-effect arrow-r"><i class="fa fa-graduation-cap"></i> Perl and Erlang <i
class="fa fa-angle-down rotate-icon"></i></a><div class="collapsible-body"><ul><li><a href="/courses/lang/index.html"
title="Kurser om programmering i Erlang och Perl" class="course waves-effect">Översikt av 2 kurser</a></li><li><a
href="/courses/lang/erlang.html" title="Kurs i Erlang programmering" class="course waves-effect">Erlang</a></li><li><a
href="/courses/lang/perl.html" title="Kurs i Perl programmering" class="course waves-effect">Perl</a></li></ul></div></li><li><a
 title="Kurser om utveckling av moderna webbapplikationer" class="topic-name collapsible-header waves-effect arrow-r"><i
class="fa fa-graduation-cap"></i> Web Apps Dev. <i class="fa fa-angle-down rotate-icon"></i></a><div class="collapsible-body">
<ul><li><a href="/courses/web/index.html" title="Kurser om utveckling av moderna webbapplikationer" class="course waves-effect">
Översikt av 2 kurser</a></li><li><a href="/courses/web/angular-basics.html"
title="Kurs i utveckling av webbapplikationer med Angular" class="course waves-effect">Angular</a></li><li><a
href="/courses/web/bootstrap.html" title="Kurs i att designa webbapplikationer med Twitter Bootstrap"
class="course waves-effect">Twitter Bootstrap</a></li></ul></div></li></ul></li></ul><div class="sidenav-bg mask-strong"></div>
</div><nav class="navbar fixed-top navbar-toggleable-md navbar-expand-lg scrolling-navbar double-nav"><div class="float-left"><a
 href="#" data-activates="slide-out" class="button-collapse"><i class="fa fa-bars"></i></a></div><div
class="breadcrumb-dn mr-auto"><p class="slogan">Kurser i Programmering för Utvecklare</p></div><ul
class="nav navbar-nav nav-flex-icons ml-auto" id="pages-list"><li class="nav-item" title="Start/Välkomstsidan"><a
class="nav-link hoverable" href="/index.html"><i class="fa fa-home"></i> <span class="clearfix d-none d-sm-inline-block">Hem
</span></a></li><li class="nav-item" title="Schema för våra klassrumskurser, vilka ges i centrala Stockholm"><a
class="nav-link hoverable" href="/pages/schedule.html"><i class="fa fa-calendar"></i> <span
class="clearfix d-none d-sm-inline-block">Kursschema</span></a></li><li class="nav-item"
title="Vår prismodell för företagsinterna kurser"><a class="nav-link hoverable" href="/pages/prices.html"><i
class="fa fa-money"></i> <span class="clearfix d-none d-sm-inline-block">Priser</span></a></li><li class="nav-item"
title="Vanliga frågor och svar"><a class="nav-link hoverable" href="/pages/faq.html"><i class="fa fa-question"></i> <span
class="clearfix d-none d-sm-inline-block">Vanliga frågor</span></a></li><li class="nav-item" title="Vår kontaktinformation"><a
class="nav-link hoverable" href="/pages/contact.html"><i class="fa fa-info-circle"></i> <span
class="clearfix d-none d-sm-inline-block">Kontakt</span></a></li><li class="nav-item"><a
href="https://www.ribomation.com/index.html" hreflang="en" class="nav-link" id="lang-flag"><img src="img/flags/en.png"
title="Show in English"></a></li></ul></nav></header><main><div class="container-fluid mt-2"><h1
class="text-center h1-responsive">Kurser i Programmering för Utvecklare</h1><section id="widgets"><div class="row"><div
class="col"><a href="/pages/schedule.html"><div class="card teal darken-3 text-center z-depth-2"><div
class="card-body white-text"><i class="fa fa-calendar fa-2x"></i><h2>Schemalagda Kurser</h2><p class="m-0 p-1">
Centralt i Stockholm City</p></div></div></a></div><div class="col"><a href="/pages/prices.html"><div
class="card teal darken-3 text-center z-depth-2"><div class="card-body white-text"><i class="fa fa-money fa-2x"></i><h2>
Transparenta Priser</h2><p class="m-0 p-1">Enkelt att se hur mycket det kostar</p></div></div></a></div><div class="col"><a
href="/pages/prices.html#onsite"><div class="card teal darken-3 text-center z-depth-2"><div class="card-body white-text"><i
class="fa fa-plane fa-2x"></i><h2>Företagsinterna Kurser</h2><p class="m-0 p-1">Vi håller kursen hos er</p></div></div></a>
</div></div></section><section id="slider" class="mt-3 mb-2"><div id="slider-pane" class="carousel slide carousel-fade"
data-ride="carousel"><div class="carousel-inner" role="listbox"><div class="carousel-item active"><div class="view"><img
class="d-block w-100" src="/img/slider/hands-coffee-smartphone-technology.jpg" style="height: 15rem; object-fit: none;"><div
class="mask rgba-black-light"></div></div><div class="carousel-caption"><h3>Scrum, Designmönster, Clean Code, Agil Utveckling
</h3><p>Agil programutveckling handlar primärt om att undanröja distraherande faktorer</p></div></div><div
class="carousel-item"><div class="view"><img class="d-block w-100" src="/img/slider/hands-woman-laptop-notebook.jpg"
style="height: 15rem; object-fit: none;"><div class="mask rgba-black-light"></div></div><div class="carousel-caption"><h3>
GulpJS, Gradle, Make, Maven, Ant</h3><p>
Att kunna bygga applikationer på ett reproducerbart sätt med såväl kompilering, artifaktbyggnation och testning är viktiga beståndsdelar av ett modernt byggverktyg
</p></div></div><div class="carousel-item"><div class="view"><img class="d-block w-100"
src="/img/slider/man-coffee-cup-pen.jpg" style="height: 15rem; object-fit: none;"><div class="mask rgba-black-light"></div>
</div><div class="carousel-caption"><h3>C++ Programmering</h3><p>
Den nya standarden C++ 11/14/17 gör oss helt lyriska, då C++ nu har lambda expressions, multi-assignment, regex, threads och mycket mera.
</p></div></div><div class="carousel-item"><div class="view"><img class="d-block w-100" src="/img/slider/startup-photos-2.jpg"
style="height: 15rem; object-fit: none;"><div class="mask rgba-black-light"></div></div><div class="carousel-caption"><h3>
C Programmering</h3><p>Programspråket C är ett givet val vid maskinnära programmering</p></div></div><div class="carousel-item">
<div class="view"><img class="d-block w-100" src="/img/slider/people-office-group-team.jpg"
style="height: 15rem; object-fit: none;"><div class="mask rgba-black-light"></div></div><div class="carousel-caption"><h3>
Cloud Computing</h3><p>Den text du läser just nu har hämtats från Amazon CloudFront/S3 och Google FireBase</p></div></div><div
class="carousel-item"><div class="view"><img class="d-block w-100" src="/img/slider/pexels-photo-2.jpg"
style="height: 15rem; object-fit: none;"><div class="mask rgba-black-light"></div></div><div class="carousel-caption"><h3>
Groovy, Grails, Spock</h3><p>
Groovy kan beskrivas som &quot;Java++&quot;, dvs en utökad och förbättrad version av Java som minskar kodmängden och ökar kodproduktiviteten
</p></div></div><div class="carousel-item"><div class="view"><img class="d-block w-100" src="/img/slider/pexels-photo-3.jpg"
style="height: 15rem; object-fit: none;"><div class="mask rgba-black-light"></div></div><div class="carousel-caption"><h3>
Java Programmering, REST-WS, Threads, JUnit</h3><p>Java är det primära språket för utveckling av server-side applikationer</p>
</div></div><div class="carousel-item"><div class="view"><img class="d-block w-100" src="/img/slider/pexels-photo-4.jpg"
style="height: 15rem; object-fit: none;"><div class="mask rgba-black-light"></div></div><div class="carousel-caption"><h3>
JavaScript, TypeScript och NodeJS</h3><p>
JavaScript är på väg att bli det mest populära och tillämpade språket, eftersom det användas på klient-sidan, server-sidan och på databas-sidan.
</p></div></div><div class="carousel-item"><div class="view"><img class="d-block w-100" src="/img/slider/startup-photos-2.jpg"
style="height: 15rem; object-fit: none;"><div class="mask rgba-black-light"></div></div><div class="carousel-caption"><h3>
Android, Ionic</h3><p>Native eller hybrid mobile app, det är den första frågan när man ska utveckla för smartphones</p></div>
</div><div class="carousel-item"><div class="view"><img class="d-block w-100" src="/img/slider/pexels-photo.jpg"
style="height: 15rem; object-fit: none;"><div class="mask rgba-black-light"></div></div><div class="carousel-caption"><h3>
Erlang och Perl Programmering</h3><p>Att kunna flera programspråk är en dygd för varje professionell programutvecklare</p></div>
</div><div class="carousel-item"><div class="view"><img class="d-block w-100"
src="/img/slider/technology-computer-chips-gigabyte.jpg" style="height: 15rem; object-fit: none;"><div
class="mask rgba-black-light"></div></div><div class="carousel-caption"><h3>Angular, Twitter Bootstrap</h3><p>
SPA (Single-Page Application) webbapplikationer är standard numera och Angular är standard/ramverket för utveckling av SPA</p>
</div></div><div class="carousel-item"><div class="view"><img class="d-block w-100"
src="/img/slider/hands-woman-laptop-notebook.jpg" style="height: 15rem; object-fit: none;"><div class="mask rgba-black-light">
</div></div><div class="carousel-caption"><h3>GIT, Jenkins och BASH skripts</h3><p>
Versionshantering såsom GIT och CI/CD servrar såsom Jenkins är essentiella komponenter i en modern programutvecklingsprocess</p>
</div></div></div><a class="carousel-control-prev" href="#slider-pane" role="button" data-slide="prev"><span
class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a><a
class="carousel-control-next" href="#slider-pane" role="button" data-slide="next"><span class="carousel-control-next-icon"
aria-hidden="true"></span> <span class="sr-only">Next</span></a></div></section><section id="info" class="mt-3 mb-2"><div
class="row"><div class="col"><div class="card"><div class="card-body text-center z-depth-2"><h2 class="card-title blue-text">
Praktiskt inriktade kurser</h2><h5 class="">
Vi vet av egen och andras erfarenhet att det många gånger är svårt att hinna med allt man vill. Våra kurser är därför koncentrerade, praktiskt inriktade och alltid up-to-date. För det mesta, gör vi en översikt/justering av kurs-materialet inför varje kurs-tillfälle.
</h5></div></div></div><div class="col"><div class="card"><div class="card-body text-center z-depth-2"><h2
class="card-title blue-text">Författade av oss</h2><h5 class="">
Vårt kursmaterial är alltid författat av oss själva och baserat på egna erfarenhet och insikter i ämnet. Det skrivna materialet är på engelska för att passa språk-blandade grupper, men själva presentationen görs på svenska eller engelska, beroende på deltagarnas önskemål.
</h5></div></div></div></div></section><section id="testimonials" class="mt-3 mb-2"><div class="row"><div class="col-1"><i
class="fa fa-quote-right fa-5x green-text darken-3"></i></div><div id="testimonials-pane" class="col text-center"><div><cite>
Mycket bra! Många vitala delar behandlades, mycket bra med de praktiska övningarna!</cite><address class="text-right">-- <em>
Satisfied participant on</em> Modern C++ for Java Developers</address></div><div><cite>
This was the best course that I have taken so far.</cite><address class="text-right">-- <em>Satisfied participant on</em>
 Linux Systems Programming</address></div><div><cite>Mycket bra kurs, kan varmt rekommederas för Java utvecklare.</cite><address
 class="text-right">-- <em>Satisfied participant on</em> Modern C++ for Java Developers</address></div><div><cite>
One of the best courses I have attended.</cite><address class="text-right">-- <em>Satisfied participant on</em>
 Threads Programming using C++</address></div><div><cite>High pace, but fun. Nice and competent teacher.</cite><address
class="text-right">-- <em>Satisfied participant on</em> Erlang Basics</address></div><div><cite>
The quality of the course exceeded my expectation. Very good course! Very good teacher!</cite><address class="text-right">--
<em>Satisfied participant on</em> Linux Systems Programming</address></div><div><cite>
Välskrivet kursmaterial, duktig föreläsare och de praktiska övningarna.</cite><address class="text-right">-- <em>
Satisfied participant on</em> Modern C++ for Java Developers</address></div><div><cite>Perfect teacher, knows everything.</cite>
<address class="text-right">-- <em>Satisfied participant on</em> Linux Systems Programming</address></div><div><cite>
The teacher has excellent knowledge regarding the course.</cite><address class="text-right">-- <em>Satisfied participant on</em>
 Linux Systems Programming</address></div><div><cite>Kunnig föreläsare, bra kursmaterial och övningar.</cite><address
class="text-right">-- <em>Satisfied participant on</em> Modern C++ for Java Developers</address></div><div><cite>
En av de bästa kurserna jag gått. Bra balans mellan teori och praktik.</cite><address class="text-right">-- <em>
Satisfied participant on</em> Android Kickstart</address></div><div><cite>Mycket bra och kunnig lärare.</cite><address
class="text-right">-- <em>Satisfied participant on</em> Advanced C++</address></div><div><cite>Duktig, pedagogisk.</cite>
<address class="text-right">-- <em>Satisfied participant on</em> Advanced C++</address></div><div><cite>
Kunnig, pedagogisk och vettig.</cite><address class="text-right">-- <em>Satisfied participant on</em> Linux Systems Programming
</address></div><div><cite>Utmärkta exempel.</cite><address class="text-right">-- <em>Satisfied participant on</em> Advanced C++
</address></div><div><cite>Amazing teacher with very deep and relevant knowledge of the subject.</cite><address
class="text-right">-- <em>Satisfied participant on</em> Linux Systems Programming</address></div></div><div class="col-1"></div>
</div></section><section id="customers" class="mt-3 mb-2"><div class="row"><div class="col"><div class="card"><div
class="card-body text-center z-depth-2"><h2 class="card-title blue-text mb-0">Kunder</h2><p class="card-text mt-0">
Här listar vi några av våra kunder vi hjälpt med att vässa sin kompetens.</p><p id="customers-pane"><span class="mx-2">21 Grams
</span> <span class="mx-2">Add Skills</span> <span class="mx-2">Agero</span> <span class="mx-2">Arctic Group</span> <span
class="mx-2">Atlas Copco</span> <span class="mx-2">BRP Systems</span> <span class="mx-2">C4 Contexture</span> <span
class="mx-2">CA</span> <span class="mx-2">Cale Access</span> <span class="mx-2">Cinnober</span> <span class="mx-2">EVRY</span>
<span class="mx-2">Ericsson</span> <span class="mx-2">Etteplan</span> <span class="mx-2">FLIR Systems</span> <span class="mx-2">
Folkhälsomyndigheten</span> <span class="mx-2">Fujitsu</span> <span class="mx-2">Försäkringskassan</span> <span class="mx-2">
Handelsbanken</span> <span class="mx-2">HiQ</span> <span class="mx-2">IAR Systems</span> <span class="mx-2">Informator</span>
<span class="mx-2">KnowIT</span> <span class="mx-2">Kronofogdemyndigheten</span> <span class="mx-2">Load Impact</span> <span
class="mx-2">Mojang</span> <span class="mx-2">Nixdorf</span> <span class="mx-2">Nokia</span> <span class="mx-2">Omegapoint
</span> <span class="mx-2">Polaris</span> <span class="mx-2">ProposalsFactory</span> <span class="mx-2">Pulsen</span> <span
class="mx-2">Quinyx</span> <span class="mx-2">Rely IT</span> <span class="mx-2">Riada</span> <span class="mx-2">SAS</span> <span
 class="mx-2">SEB</span> <span class="mx-2">SPP</span> <span class="mx-2">Scania</span> <span class="mx-2">Sjöland&amp;Thyselius
</span> <span class="mx-2">Skandia</span> <span class="mx-2">Skatteverket</span> <span class="mx-2">Smittskyddsinstitutet</span>
 <span class="mx-2">Specialpedagogiska Skolmyndigheten</span> <span class="mx-2">Svensk Bilprovning</span> <span class="mx-2">
Svenska Kyrkan</span> <span class="mx-2">Swedbank</span> <span class="mx-2">Systemstöd</span> <span class="mx-2">Telia</span>
<span class="mx-2">Tieto</span> <span class="mx-2">Tightly Knit Consulting</span> <span class="mx-2">Trygghetsrådet</span> <span
 class="mx-2">Upbase</span> <span class="mx-2">Uppsala Universitet</span> <span class="mx-2">Viaduct</span> <span class="mx-2">
Visma</span> <span class="mx-2">Volvo Car Corporation</span> <span class="mx-2">Volvo Construction Equipment</span> <span
class="mx-2">iZettle</span> <span class="mx-2">ÅF Consult</span> <span class="mx-2">Ålandsbanken</span> <span class="mx-2">PRV
</span></p></div></div></div></div></section><div class="mb-5">&nbsp;</div></div></main><footer
class="page-footer fixed-bottom blue-ic mt-4"><div class="footer-copyright text-center py-3"><div class="container">
© 2009-2018 Copyright: <a href="https://www.ribomation.se/" class="hoverable">Ribomation AB, Stockholm/Sweden. </a><span
class="float-right"><a href="/pages/gdpr.html" class="text-light hoverable">(Our privacy policy - GDPR)</a></span></div></div>
</footer><script async src="https://www.google-analytics.com/analytics.js"></script><script src="/js/jquery-3.3.1.min.js">
</script><script src="/js/popper.min.js"></script><script src="/js/bootstrap.min.js"></script><script src="/js/mdb.min.js">
</script><script src="/js/site.js"></script><script type="text/javascript" src="/js/front.js"></script></body></html>