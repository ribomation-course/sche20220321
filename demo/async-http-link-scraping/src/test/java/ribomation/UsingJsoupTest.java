package ribomation;

import org.jsoup.Jsoup;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class UsingJsoupTest {
    final String baseUrl        = "https://www.ribomation.se";
    final String startPagePath  = "/start-page.txt";
    final String coursePagePath = "/course-page.txt";

    String startPage, coursePage;

    String load(String path) throws IOException {
        var is = getClass().getResourceAsStream(path);
        assertNotNull(is);
        var buf = new ByteArrayOutputStream();
        is.transferTo(buf);
        return buf.toString();
    }

    @BeforeEach
    void load_start() throws IOException {
        startPage = load(startPagePath);
        coursePage = load(coursePagePath);
    }


    @Test
    void just_parsing() {
        var doc = Jsoup.parse(startPage, baseUrl);
        assertNotNull(doc);
    }

    @Test
    void extracting() {
        var doc   = Jsoup.parse(startPage, baseUrl);
        var links = doc.select("#courses-list a.course");
        assertNotNull(links);

        var urls = links.stream()
                .map(link -> link.attr("href"))
                .filter(url -> !url.endsWith("index.html"))
                .map(uri -> baseUrl + uri)
                .collect(Collectors.toList());
        assertEquals(43, urls.size());
        //urls.forEach(System.out::println);
    }

    @Test
    void ingress() {
        var doc = Jsoup.parse(coursePage, baseUrl);
        var txt = doc.select(".ingress p").text();
        assertNotNull(txt);
        txt = txt.substring(0, Math.min(txt.length(), 100));
        //System.out.printf("txt: %s%n", txt);
    }

    @Test
    void name() {
        var doc = Jsoup.parse(coursePage, baseUrl);
        var txt = doc.select("#facts th:contains(Namn) + td").text();
        assertNotNull(txt);
        //System.out.printf("txt: %s%n", txt);
    }

    @Test
    void duration() {
        var doc = Jsoup.parse(coursePage, baseUrl);
        var txt = doc.select("#facts th:contains(Längd) + td").text();
        assertNotNull(txt);
        //System.out.printf("txt: %s%n", txt);
    }

}

