package ribomation;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertIterableEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class UsingStreamsTest {

    @Test
    @DisplayName("tapping into a stream a 2nd time, should throw")
    void multi_stream() {
        var ints = IntStream.rangeClosed(1, 5).boxed();

        var square = ints.map(n -> n * n).collect(Collectors.toList());
        assertIterableEquals(Arrays.asList(1, 4, 9, 16, 25), square);

        assertThrows(IllegalStateException.class, () -> {
                ints.map(n -> n * 2).collect(Collectors.toList());
        });
    }

}
