package ribomation.java8.simple_fetch;
import org.asynchttpclient.Response;

public abstract class BaseStuff {
    static final String url = "https://www.ribomation.se/java/java-8.html";

    public void print(Response response) {
        if (response.getStatusCode() == 200 && response.hasResponseBody()) {
            System.out.printf("--- RESPONSE ---%n%s%n",
                    response.getResponseBody());
        }
    }

}
