package ribomation.rest_ws.client;

import java.io.IOException;
import java.net.URI;
import java.net.http.*;
import static java.net.http.HttpClient.Redirect.*;
import static java.net.http.HttpResponse.BodyHandlers.*;
import static java.time.Duration.*;

public class SimpleRequest {
    public static void main(String[] args) throws Exception {
        SimpleRequest app = new SimpleRequest();
        app.run();
    }

    public void run() throws IOException, InterruptedException {
        var client = HttpClient.newBuilder()
                .followRedirects(NORMAL)
                .connectTimeout(ofSeconds(5))
                .build();

        var req = HttpRequest.newBuilder()
                .uri(URI.create("https://www.google.se/robots.txt"))
                .GET()
                .build();

        var res = client.send(req, ofLines());
        res.body()
                .filter(line -> line.startsWith("Allow"))
                .limit(10)
                .forEach(System.out::println);
    }
}


