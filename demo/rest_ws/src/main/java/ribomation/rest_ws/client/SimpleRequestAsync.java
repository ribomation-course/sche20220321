package ribomation.rest_ws.client;

import java.net.URI;
import java.net.http.*;
import static java.net.http.HttpClient.Redirect.*;
import static java.net.http.HttpResponse.BodyHandlers.*;
import static java.time.Duration.*;

public class SimpleRequestAsync {
    public static void main(String[] args) {
        SimpleRequestAsync app = new SimpleRequestAsync();
        app.run();
    }

    public void run() {
        var client = HttpClient.newBuilder()
                .followRedirects(NORMAL)
                .connectTimeout(ofSeconds(5))
                .build();

        var req = HttpRequest.newBuilder()
                .uri(URI.create("https://www.ribomation.se/robots.txt"))
                .GET()
                .build();

        client.sendAsync(req, ofString())
                .thenApply(HttpResponse::body)
                .thenAccept(System.out::println)
                .join();
    }

}
