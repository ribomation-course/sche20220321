package ribomation.records;

public record Person(String firstName, String lastName, int age) {
    public String lastName() {
        return lastName.toUpperCase();
    }

    public String name() {
        return firstName + " " + lastName;
    }
}


