package ribomation.records;

public record Point(int x, int y) implements Comparable<Point> {
    public Point {
        if (x < 0 || y < 0) {
            throw new IllegalArgumentException(String.format("Must be non-negative [%d, %d]", x, y));
        }
    }

    @Override
    public int compareTo(Point that) {
        if (this.x != that.x) return Integer.compare(this.x, that.x);
        return Integer.compare(this.y, that.y);
    }
}



