package ribomation.records;

import java.util.List;
import java.util.TreeSet;

public class PointApp {
    public static void main(String[] args) {
        var app = new PointApp();
        app.run();
    }

    void run() {
        var c1 = new Point(3, 5);
        var c2 = new Point(3, 5);
        var c3 = new Point(3, 7);
        System.out.printf("%s == %s -> %b%n", c1, c2, c1.equals(c2));
        System.out.printf("%s == %s -> %b%n", c1, c3, c1.equals(c3));

        var lst = List.of(c1, c2, c3, new Point(1, 7));
        System.out.printf("sorted: %s%n", new TreeSet<>(lst));

        try {
            var c = new Point(5, -5);
        } catch (Exception e) {
            System.out.printf("err: %s%n", e.getMessage());
        }
    }


}
