package ribomation.records;

public class ProductApp {
    public static void main(String[] args) {
        var app = new ProductApp();
        app.run();
    }

    void run() {
        var apple = new Product("Apple", 5);
        var banana = new Product("Banana", 3);

        System.out.printf("apple.string: %s%n", apple);
        System.out.printf("banana.string: %s%n", banana);

        System.out.printf("(a) VAT=%d%% %n", Product.VAT());
        System.out.printf("apple.price: %s%n", apple.price());
        System.out.printf("banana.price: %s%n", banana.price());

        Product.setVAT(10);
        System.out.printf("(b) VAT=%d%% %n", Product.VAT());
        System.out.printf("apple.price: %s%n", apple.price());
        System.out.printf("banana.price: %s%n", banana.price());
    }

}
