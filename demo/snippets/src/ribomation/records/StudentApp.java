package ribomation.records;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Function;
import java.util.function.Supplier;

public class StudentApp {
    record Student(String name) { }
    record Result(Student student, String course, int grade) { }

    void printTopStudents(List<Student> students, List<Result> results, int max) {
        record Grade(Student student, double avgGrade) { }

        Function<Student, Double> avgGradeOf = s ->
                results.stream()
                        .filter(r -> r.student().name().equals(s.name()))
                        .mapToInt(Result::grade)
                        .average()
                        .orElse(0);

        students.stream()
                .map(s -> new Grade(s, avgGradeOf.apply(s)))
                .sorted((lhs, rhs) -> Double.compare(rhs.avgGrade(), lhs.avgGrade()))
                .limit(max)
                .forEach(g -> System.out.printf("%-12s: %.1f%n", g.student().name(), g.avgGrade()));
    }

    List<Student> loadStudents() {
        return List.of(
                new Student("Anna Conda"),
                new Student("Ana Gram"),
                new Student("Per Silja"),
                new Student("Inge Vidare"),
                new Student("Sham Poo"),
                new Student("Justin Time"),
                new Student("K.A. Ramell"),
                new Student("Per Forerad"),
                new Student("Barb E. Cue"),
                new Student("Carl Arm"),
                new Student("Cory Ander"),
                new Student("Dee Zaster"),
                new Student("Hammond Eggs")
        );
    }

    List<String> loadCourses() {
        return List.of(
                "Java Basics", "Java Intermediate", "Java 8+", "Java REST-WS", "Java Concurrency",
                "JUnit Testing", "Spock Testing", "Maven Build Tool", "Gradle Build Tool"
        );
    }

    List<Result> loadResult(List<Student> students, List<String> courses, int n) {
        var r = new Random(System.nanoTime() % 997);
        Supplier<Result> mk = () -> {
            var student = students.get(r.nextInt(students.size()));
            var course = courses.get(r.nextInt(courses.size()));
            var grade = 1 + r.nextInt(10);
            return new Result(student, course, grade);
        };
        var result = new ArrayList<Result>();
        while (n-- > 0) result.add(mk.get());
        return result;
    }

    void run() {
        var students = loadStudents();
        var courses = loadCourses();
        var results = loadResult(students, courses, 100);
        printTopStudents(students, results, 6);
    }

    public static void main(String[] args) {
        var app = new StudentApp();
        app.run();
    }
}
