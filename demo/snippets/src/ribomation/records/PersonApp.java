package ribomation.records;

public class PersonApp {
    public static void main(String[] args) {
        var app = new PersonApp();
        app.run();
    }

    void run() {
        var p = new Person("Nisse", "Hult", 42);
        System.out.printf("p.string: %s%n", p);
        System.out.printf("p.name: %s%n", p.name());
        System.out.printf("p.last: %s%n", p.lastName());
    }


}
