package ribomation.using_instance_of;

public class Person implements Comparable<Person> {
    String name;
    int age;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String toString() { return String.format("Person{%s, %d}", name, age); }

    @Override
    public boolean equals(Object o) {
        return (o instanceof Person that) && this.name.equals(that.name) && this.age == that.age;
    }

    @Override
    public int hashCode() { return 31 * name.hashCode() + age; }

    @Override
    public int compareTo(Person that) {
        if (this.name.equals(that.name)) return Integer.compare(this.age, that.age);
        return this.name.compareTo(that.name);
    }
}

