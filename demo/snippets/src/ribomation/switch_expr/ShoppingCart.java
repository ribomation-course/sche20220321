package ribomation.switch_expr;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
    class Item {
        final String product;
        final int count;
        final float price;

        Item(String product, int count, float price) {
            this.product = product;
            this.count = count;
            this.price = price;
        }

        float total() {
            return count * price;
        }
    }

    final List<Item> items = new ArrayList<>();

    float total() {
        return (float) items.stream().mapToDouble(Item::total).sum();
    }

    String status(int count) {
        return switch (count) {
            case 0 -> "Zero products";
            case 1 -> "One product";
            case 2,3,4 -> "Few products";
            default -> "Many products";
        } ;
    }

    public static void main(String[] args) {
        var app = new ShoppingCart();
        System.out.printf("%s%n", app.status(0));
        System.out.printf("%s%n", app.status(1));
        System.out.printf("%s%n", app.status(4));
        System.out.printf("%s%n", app.status(5));
    }

}
