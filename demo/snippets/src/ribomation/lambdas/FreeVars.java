package ribomation.lambdas;

import java.util.function.UnaryOperator;

public class FreeVars {
    public static void main(String[] args) {
        var app = new FreeVars();
        app.run();
        app.run2();
    }

    void run() {
        int a = 3, b = 21;
        UnaryOperator<Integer> f = x -> a * x + b;
        int n = 7;
        System.out.printf("(a) f(%d) = %d%n", n, f.apply(n));
    }

    void run2() {
        final int[] prod = {1};
        UnaryOperator<Integer> f = x -> prod[0] = x * prod[0];
        int n = 5;
        for (int k = 1; k <= n; ++k) f.apply(k);
        System.out.printf("(b) f(%d) = %d%n", n, prod[0]);
    }

}
