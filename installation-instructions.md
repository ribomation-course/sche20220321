# Installation Instructions

In order to participate and perform the programming exercises of the course, 
you need to have the following installed.

## Zoom Client
* https://us02web.zoom.us/download
* [Video/Audio Configuration](https://gitlab.com/ribomation-course/common-instructions/-/blob/master/zoom-configuration.md)

## GIT Client
* https://git-scm.com/downloads

## IDE
A decent IDE, such as any of

* JetBrains IntellJ IDEA (*Community*)
    - https://www.jetbrains.com/idea/download
* MicroSoft Visual Code
    - https://code.visualstudio.com/download


## Java JDK
The latest version of Java JDK installed. Currently, that is version 17.
However, version 18 is in EA stage and scheduled to be 
[released March 22, 2022](https://openjdk.java.net/projects/jdk/18/).
* [Java JDK Download](https://jdk.java.net/17/)

